package com.sprapp.controller;

import com.sprapp.dao.impl.UserDaoImpl;
import com.sprapp.model.User;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping("/user")
public class UserController {

    @Autowired
    private UserDaoImpl userDao;

    @RequestMapping(value = "register", method = RequestMethod.POST)
    public String addUser(@ModelAttribute("addUser") User user,
                          ModelMap model) {
        userDao.insertUser(user);
        return "redirect:/login";
    }

    @RequestMapping(value = "registration", method = RequestMethod.GET)
    public String getRegForm(ModelMap model) {
        model.addAttribute("addUser", new User());
        return "/registration";
    }
}