package com.sprapp.controller;

import com.sprapp.util.LoginForm;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class LoginController {
    private static final String LOGIN_VIEW = "login";

    @RequestMapping("/login")
    public String login(LoginForm loginForm) {
        Logger logger = LoggerFactory.getLogger(LoginController.class);
        logger.info("Init Class");
        return LOGIN_VIEW;
    }
}
