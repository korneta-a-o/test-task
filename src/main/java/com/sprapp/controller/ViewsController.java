package com.sprapp.controller;

import org.springframework.security.authentication.AnonymousAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class ViewsController {

    @RequestMapping(value = "403", method = RequestMethod.GET)
    public ModelAndView accessDenied() {

        ModelAndView model = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetail = (UserDetails)auth.getPrincipal();
            model.addObject("username", userDetail.getUsername());
            model.addObject("authorities", userDetail.getAuthorities());
        }

        model.setViewName("403");
        return model;
    }

    @RequestMapping(value = "general", method = RequestMethod.GET)
    public String pieceOf(ModelMap model) {
        return "result";
    }

    @RequestMapping(value = "/", method = RequestMethod.GET)
    public String getIndex(ModelMap model) {
        return "index";
    }

    @RequestMapping(value = "header", method = RequestMethod.GET)
    public ModelAndView getHeader() {
        ModelAndView model = new ModelAndView();
        Authentication auth = SecurityContextHolder.getContext().getAuthentication();
        if (!(auth instanceof AnonymousAuthenticationToken)) {
            UserDetails userDetails = (UserDetails)auth.getPrincipal();
            model.addObject("isAuthenticated", userDetails.getUsername());
        } else model.addObject("isAuthenticated", "anonymous");
        model.setViewName("/template/header");
        return model;
    }

    @RequestMapping(value = "head", method = RequestMethod.GET)
    public String getHead(ModelMap modelMap) {
        return "/template/head";
    }

    @RequestMapping(value = "footer", method = RequestMethod.GET)
    public String getFooter(ModelMap modelMap) {
        return "/template/footer";
    }

}
