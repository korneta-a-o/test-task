package com.sprapp.controller;

import com.sprapp.dao.UserDao;
import com.sprapp.exception.NotFoundException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@Controller
@RequestMapping(value = "result")
public class ResultController {
    @Autowired
    private        UserDao           userDao;
    private static int               answersValue;

    @RequestMapping(value = "mark", method = RequestMethod.GET)
    public String filtered(ModelMap model) throws NotFoundException {
        final String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        model.addAttribute("user", userDao.getUserByLogin(userLogin).getName());
        model.addAttribute("testResult", answersValue);
        return "result";
    }

    public static void setAnswersValue(int answersValue) {
        ResultController.answersValue = answersValue;
    }
}
