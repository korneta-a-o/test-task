package com.sprapp.controller;

import com.sprapp.dao.AnswerDao;
import com.sprapp.dao.InterpretationDao;
import com.sprapp.dao.QuestionDao;
import com.sprapp.dao.ResultDao;
import com.sprapp.dao.TestDao;
import com.sprapp.dao.UserDao;
import com.sprapp.dao.VariantDao;
import com.sprapp.exception.ConflictException;
import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Answer;
import com.sprapp.model.Question;
import com.sprapp.model.Result;
import com.sprapp.model.Test;
import com.sprapp.model.Variant;
import com.sprapp.util.QuestionsContainer;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Controller
@RequestMapping(value = "test")
public class TestController {
    @Autowired
    private TestDao           testDao;
    @Autowired
    private UserDao           userDao;
    @Autowired
    private QuestionDao       questionDao;
    @Autowired
    private VariantDao        variantDao;
    @Autowired
    private InterpretationDao interpretationDao;
    @Autowired
    AnswerDao answerDao;
    @Autowired
    private ResultDao resultDao;

    private final Map<Integer, QuestionsContainer> questionsContainers = new HashMap<>();

    @RequestMapping(value = "all", method = RequestMethod.GET)
    public String getAllTests(ModelMap model) {
        List<Test> allTests = testDao.getAll();
        model.addAttribute("allTests", allTests);
        return "tests";
    }

    @RequestMapping(value = "{testId}/start", method = RequestMethod.GET)
    public String startTest(@PathVariable Integer testId,
                            ModelMap model) throws Exception {
        final String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        Integer userId = userDao.getUserByLogin(userLogin).getIdUser();

        if (!questionsContainers.containsKey(userId)) {
            questionsContainers.put(userId, new QuestionsContainer());
        } else if (questionsContainers.get(userId).isUserStartTest(testId)) {
            return "redirect:/test/" + testId + "/question";
        }

        List<Question> questions = questionDao.getQuestionsByIdTest(testId);

        Result result = new Result();
        result.setIdTest(testId);
        result.setIdUser(userId);
        resultDao.insert(result);

        questionsContainers.get(userId).startTest(testId, result.getIdResult(), questions);

        return "redirect:/test/" + testId + "/question";
    }

    @RequestMapping(value = "{testId}/stop", method = RequestMethod.GET)
    public String stopTest(ModelMap modelMap, @PathVariable Integer testId) throws NotFoundException {
        Integer currentUserId = getCurrentUserId();
        Integer resultId = questionsContainers.get(currentUserId).stopTest(testId);

        List<Answer> answers = answerDao.getAnswersByResultId(resultId);

        Map<Integer, Integer> qwe = new HashMap<>();

        for (Answer answer : answers) {
            Variant variant = variantDao.getVariantById(answer.getIdVariant());

            int idInterpretation = variant.getIdInterpretation();
            if (!qwe.containsKey(idInterpretation)) {
                qwe.put(idInterpretation, 1);
            } else {
                qwe.put(idInterpretation, qwe.get(idInterpretation) + 1);
            }
        }

        int mark = 0;
        for (Map.Entry<Integer, Integer> answerCounter : qwe.entrySet()) {
            mark += answerCounter.getValue() * Integer.parseInt(interpretationDao
                                                                        .getById(answerCounter.getKey())
                                                                        .getDescription());
        }
        ResultController.setAnswersValue(mark);
        return "redirect:/result/mark";
    }

    @RequestMapping(value = "{testId}/question", method = RequestMethod.GET)
    public String getQuestion(@PathVariable Integer testId,
                              @RequestParam(required = false) boolean skip,
                              ModelMap model) throws NotFoundException, ConflictException {
        final String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        Integer userId = userDao.getUserByLogin(userLogin).getIdUser();

        if (!questionsContainers.containsKey(userId)) {
            return "redirect:/test/" + testId + "/started";
        }

        QuestionsContainer questionsContainer = questionsContainers.get(userId);

        if (skip) {
            questionsContainer.skipTest(testId);
        }

        if (questionsContainer.hasQuestion(testId)) {
            Question question = questionsContainer.getQuestion(testId);
            model.putAll(setModel(question, testId, userId, questionsContainer.getResultId(testId)));
        } else {
            return "redirect:/test/" + testId + "/stop";
        }

        return "questions";
    }

    @RequestMapping(value = "answer", method = RequestMethod.POST)
    public String saveAnswer(@ModelAttribute("addAnswer") Answer answer) throws NotFoundException {
        if (!(answer.getIdVariant() == 0)) {
            questionsContainers.get(getCurrentUserId()).dropQuestion(answer.getIdTest());
            answerDao.insert(answer);
        } else {
            System.err.print("answer has not variant");
        }
        return "redirect:/test/" + String.valueOf(answer.getIdTest()) + "/question";
    }

    @RequestMapping(value = "{testId}/started", method = RequestMethod.GET)
    public String getAlreadyStartedPage(ModelMap modelMap, @PathVariable Integer testId) throws NotFoundException {
        modelMap.addAttribute("restoreInterruptTest", "ASD");
        modelMap.addAttribute("stopTest", "ASD");
        final String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        modelMap.addAttribute("user", userDao.getUserByLogin(userLogin).getName());
        return "started";
    }

    private Integer getCurrentUserId() throws NotFoundException {
        final String userLogin = SecurityContextHolder.getContext().getAuthentication().getName();
        return userDao.getUserByLogin(userLogin).getIdUser();
    }

    private static ModelMap setModel(Question question, int testId, int userId, int resultId) {
        final ModelMap model = new ModelMap();
        Collections.shuffle(question.getVariants());
        model.addAttribute("question", question);
        model.addAttribute("idTest", testId);
        Answer currentAnswer = new Answer();
        currentAnswer.setIdResult(resultId);
        currentAnswer.setIdUser(userId);
        currentAnswer.setIdQuestion(question.getIdQuestion());
        currentAnswer.setIdTest(testId);
        model.addAttribute("addAnswer", currentAnswer);
        return model;
    }
}
