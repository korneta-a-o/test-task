package com.sprapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.util.List;

@Entity
@Table(name = "question")
public class Question {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idQuestion;

    @Column(updatable = false, insertable = false, nullable = false)
    private int idTest;

    private String question;

    @OneToMany(mappedBy = "question")
    private List<Variant> variants;

    @ManyToOne
    @JoinColumn(name = "idTest")
    private Test test;

    public Question() {
    }

    public Question(String question) {
        this.question = question;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public String getQuestion() {
        return question;
    }

    public List<Variant> getVariants() {
        return variants;
    }

    public void setVariants(List<Variant> variants) {
        this.variants = variants;
    }

    public Test getTest() {
        return test;
    }

    public void setTest(Test test) {
        this.test = test;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getIdTest() {
        return idTest;
    }
}
