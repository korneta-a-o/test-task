package com.sprapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "role")
public class Role {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idRole;

    private int    idUser;
    private String role;


    public Role() {
    }

    public Role(int idUser, String role) {
        this.idUser = idUser;
        this.role = role;
    }

    public void setIdRole(int idRole) {
        this.idRole = idRole;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setRole(String role) {
        this.role = role;
    }

    public int getIdRole() {
        return idRole;
    }

    public int getIdUser() {
        return idUser;
    }

    public String getRole() {
        return role;
    }

}
