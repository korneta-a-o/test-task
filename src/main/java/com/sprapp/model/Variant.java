package com.sprapp.model;


import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Table
@Entity(name = "variant")
public class Variant {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idVariant;

    @Column(updatable = false, insertable = false, nullable = false)
    private int    idQuestion;
    private String value;
    private int    idInterpretation;

    @ManyToOne
    @JoinColumn(name = "idQuestion")
    private Question question;

    public Variant() {
    }

    public Variant(int idQuestion, String value, String isRight) {
        this.idQuestion = idQuestion;
        this.value = value;
    }

    public void setIdVariant(int idVariant) {
        this.idVariant = idVariant;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public void setValue(String value) {
        this.value = value;
    }


    public int getIdVariant() {
        return idVariant;
    }

    public int getIdQuestion() {
        return idQuestion;
    }

    public String getValue() {
        return value;
    }


    public Question getQuestion() {
        return question;
    }

    public void setQuestion(Question question) {
        this.question = question;
    }

    public int getIdInterpretation() {
        return idInterpretation;
    }

    public void setIdInterpretation(int idInterpretation) {
        this.idInterpretation = idInterpretation;
    }
}
