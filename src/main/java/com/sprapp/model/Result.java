package com.sprapp.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "result")
public class Result {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int    idResult;
    private int    idUser;
    private int    idTest;

    public Result() {
    }

    public Result(int idUser, int idTest) {
        this.idUser = idUser;
        this.idTest = idTest;
    }

    public void setIdResult(int idResult) {
        this.idResult = idResult;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public int getIdResult() {
        return idResult;
    }

    public int getIdUser() {
        return idUser;
    }

    public int getIdTest() {
        return idTest;
    }

}
