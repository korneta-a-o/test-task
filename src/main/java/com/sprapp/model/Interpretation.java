package com.sprapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "interpretation")
public class Interpretation {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idInterpretation;

    private String title;
    private String description;

    public Interpretation(String title) {
        this.title = title;
    }

    public Interpretation() {
    }

    public int getIdInterpretation() {
        return idInterpretation;
    }

    public void setIdInterpretation(int idInterpretation) {
        this.idInterpretation = idInterpretation;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
