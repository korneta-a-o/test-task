package com.sprapp.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "answer")
public class Answer {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int idAnswer;
    private int idUser;
    private int idResult;
    private int idTest;
    private int idVariant;
    private int idQuestion;

    public Answer() {
    }

    public Answer(int idUser, int idResult, int idTest, int idVariant, int idQuestion) {
        this.idUser = idUser;
        this.idResult = idResult;
        this.idTest = idTest;
        this.idVariant = idVariant;
        this.idQuestion = idQuestion;
    }

    public void setIdAnswer(int idAnswer) {
        this.idAnswer = idAnswer;
    }

    public void setIdUser(int idUser) {
        this.idUser = idUser;
    }

    public void setIdResult(int idResult) {
        this.idResult = idResult;
    }

    public void setIdTest(int idTest) {
        this.idTest = idTest;
    }

    public void setIdVariant(int idVariant) {
        this.idVariant = idVariant;
    }

    public void setIdQuestion(int idQuestion) {
        this.idQuestion = idQuestion;
    }

    public int getIdAnswer() {
        return idAnswer;
    }

    public int getIdUser() {
        return idUser;
    }

    public int getIdResult() {
        return idResult;
    }

    public int getIdTest() {
        return idTest;
    }

    public int getIdVariant() {
        return idVariant;
    }

    public int getIdQuestion() {
        return idQuestion;
    }
}
