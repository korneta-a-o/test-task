package com.sprapp.dao.impl;

import com.sprapp.dao.AnswerDao;
import com.sprapp.model.Answer;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class AnswerDaoImpl implements AnswerDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void insert(Answer answer) {
        sessionFactory.openSession()
                      .save(answer);
    }

    @Override
    public void update(Answer answer) {
        sessionFactory.openSession()
                      .update(answer);
    }

    @Override
    public void delete(Answer answer) {
        sessionFactory.openSession()
                      .delete(answer);
    }

    @Override
    public void deleteById(int idAnswer) {
        sessionFactory.openSession()
                      .delete(selectById(idAnswer));
    }

    @Override
    public Answer selectById(int idAnswer) {
        return (Answer)sessionFactory.openSession()
                                     .get(Answer.class, idAnswer);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Answer> getAnswers() {
        return sessionFactory.openSession()
                             .createCriteria(Answer.class)
                             .list();
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Answer> getAnswersByResultId(Integer resultId) {
        return sessionFactory.openSession()
                             .createQuery("from Answer where idResult = :idResult")
                             .setParameter("idResult", resultId)
                             .list();
    }
}
