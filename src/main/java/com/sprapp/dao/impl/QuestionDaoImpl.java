package com.sprapp.dao.impl;

import com.sprapp.dao.QuestionDao;
import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Question;
import com.sprapp.model.Test;

import org.hibernate.ObjectNotFoundException;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.lang.String.format;

@Repository
@Transactional
public class QuestionDaoImpl implements QuestionDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void deleteQuestion(Question question) {

    }

    @Override
    public void insertQuestion(Question question) {

    }

    @Override
    public void updateQuestion(Question question) {

    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Question> getAllQuestion() {
        return sessionFactory.openSession()
                             .createCriteria(Question.class)
                             .list();

    }

    @Override
    public List<Question> getQuestionsByIdTest(int idTest) throws NotFoundException {
        try {
            Test test = (Test)sessionFactory.openSession()
                                            .byId(Test.class)
                                            .getReference(idTest);
            return test.getQuestions();
        } catch (ObjectNotFoundException exception) {
            throw new NotFoundException(format("Test with id %s not found", idTest));
        }
    }
}
