package com.sprapp.dao.impl;

import com.sprapp.dao.ResultDao;
import com.sprapp.model.Result;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;
import java.util.List;

@Repository
@Transactional
public class ResultDaoImp implements ResultDao {

    @Autowired
    SessionFactory sessionFactory;

    @Override
    public void insert(Result result) {
        sessionFactory.openSession()
                      .save(result);
    }

    @Override
    public void update(Result result) {
        sessionFactory.openSession()
                      .update(result);
    }

    @Override
    public void delete(Result result) {
        sessionFactory.openSession()
                      .delete(result);
    }

    @Override
    public Result selectById(int idResult) {
        return (Result)sessionFactory.openSession()
                                     .get(Result.class, idResult);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Result> getResults() {
        return sessionFactory.openSession()
                             .createCriteria(Result.class)
                             .list();
    }


}
