package com.sprapp.dao.impl;

import com.sprapp.dao.VariantDao;
import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Variant;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.lang.String.*;


@Repository
@Transactional
public class VariantDaoImpl implements VariantDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void deleteVariant(Variant variant) {

    }

    @Override
    public void insertVariant(Variant variant) {

    }

    @Override
    public void updateVariant(Variant variant) {

    }

    @Override
    public List<Variant> getAllVariant() {
        return null;
    }

    @Override
    @SuppressWarnings("unchecked")
    public Variant getVariantById(int variantId) throws NotFoundException {
        List<Variant> variants = sessionFactory.openSession()
                                       .createQuery("from variant where idVariant = :idVariant")
                                       .setParameter("idVariant", variantId)
                                       .list();

        if (variants.isEmpty()) {
            throw new NotFoundException(format("Variant with id %s", variantId));
        }

        return variants.get(0);
    }
}
