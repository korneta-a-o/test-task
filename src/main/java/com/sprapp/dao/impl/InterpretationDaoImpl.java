package com.sprapp.dao.impl;

import com.sprapp.dao.InterpretationDao;
import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Interpretation;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.transaction.Transactional;

@Repository
@Transactional
public class InterpretationDaoImpl implements InterpretationDao {

    @Autowired
    private SessionFactory sessionFactory;

    @Override
    public void insert(Interpretation interpretation) {
        sessionFactory.openSession()
                      .save(interpretation);
    }

    @Override
    public void update(Interpretation interpretation) {
        sessionFactory.openSession()
                      .update(interpretation);
    }

    @Override
    public void delete(Interpretation interpretation) {
        sessionFactory.openSession()
                      .delete(interpretation);
    }

    @Override
    public void deleteById(int idInterpretation) {
        delete((Interpretation)sessionFactory
                .openSession()
                .get(Interpretation.class, idInterpretation));
    }

    @Override
    @SuppressWarnings("unchecked")
    public Interpretation getById(int idInterpretation) throws NotFoundException {
        return (Interpretation)sessionFactory.openSession()
                                             .get(Interpretation.class, idInterpretation);
    }

}
