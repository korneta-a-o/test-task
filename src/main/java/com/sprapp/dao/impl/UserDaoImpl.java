package com.sprapp.dao.impl;

import com.sprapp.exception.NotFoundException;
import com.sprapp.dao.UserDao;
import com.sprapp.model.Role;
import com.sprapp.model.User;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

import static java.lang.String.format;

@Repository
@Transactional
public class UserDaoImpl implements UserDao {

    @Autowired
    private SessionFactory sessionFactory;

    public static final String DEFAULT_ROLE = "USER";

    @Override
    public void deleteUser(User user) {
        sessionFactory
                .openSession()
                .delete(user);
    }

    @Override
    public void insertUser(User user) {
        Session session = sessionFactory.openSession();
        session.save(user);
        Role r = new Role();
        r.setRole("ROLE_" + DEFAULT_ROLE);
        r.setIdUser(user.getIdUser());
        session.save(r);
    }

    @Override
    public void updateUser(User user) {
        sessionFactory.openSession()
                      .update(user);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<User> getAllUser() {
        return sessionFactory.openSession()
                             .createCriteria(User.class)
                             .list();
    }

    @Override
    public User getUserByLogin(String userLogin) throws NotFoundException {
        List resultList = sessionFactory.openSession()
                                        .createQuery("from User where login = :login")
                                        .setParameter("login", userLogin)
                                        .list();
        if (resultList.isEmpty()) {
            throw new NotFoundException(format("User with login %s not found", userLogin));
        }

        return (User)resultList.get(0);
    }

    public void setRole(int idUser, String role) {
        //todo
    }
}
