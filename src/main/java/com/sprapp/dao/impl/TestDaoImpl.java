package com.sprapp.dao.impl;

import com.sprapp.dao.TestDao;
import com.sprapp.model.Test;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Repository
@Transactional
public class TestDaoImpl implements TestDao {

    private SessionFactory sessionFactory;

    @Autowired
    public void setSessionFactory(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void deleteTest(Test test) {
        sessionFactory.openSession()
                      .delete(test);
    }

    @Override
    public void insertTest(Test test) {
        sessionFactory.openSession()
                      .save(test);
    }

    @Override
    public void updateTest(Test test) {
        sessionFactory.openSession()
                      .update(test);
    }

    @Override
    public Test getTestById(int idTest) {
        return (Test)sessionFactory.openSession()
                                   .get(Test.class, idTest);
    }

    @Override
    @SuppressWarnings("unchecked")
    public List<Test> getAll() {
        return sessionFactory.openSession()
                             .createCriteria(Test.class)
                             .list();
    }

}
