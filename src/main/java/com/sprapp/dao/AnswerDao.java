package com.sprapp.dao;

import com.sprapp.model.Answer;

import java.util.List;

public interface AnswerDao {

    void insert(Answer answer);

    void update(Answer answer);

    void delete(Answer answer);

    void deleteById(int idAnswer);

    Answer selectById(int idAnswer);

    List<Answer> getAnswers();

    List<Answer> getAnswersByResultId(Integer resultId);
}
