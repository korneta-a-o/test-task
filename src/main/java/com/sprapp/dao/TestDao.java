package com.sprapp.dao;

import com.sprapp.model.Test;

import java.util.List;

public interface TestDao {

    void deleteTest(Test test);

    void insertTest(Test test);

    void updateTest(Test test);

    Test getTestById(int idTest);

    List<Test> getAll();

}
