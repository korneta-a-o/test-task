package com.sprapp.dao;

import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Answer;
import com.sprapp.model.Interpretation;

import java.util.List;

public interface InterpretationDao {

    void insert(Interpretation interpretation);

    void update(Interpretation interpretation);

    void delete(Interpretation interpretation);

    void deleteById(int idInterpretation);

    Interpretation getById(int idInterpretation) throws NotFoundException;

}
