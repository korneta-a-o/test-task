package com.sprapp.dao;

import com.sprapp.exception.NotFoundException;
import com.sprapp.model.User;

import java.util.List;

public interface UserDao {

    void deleteUser(User user);

    void insertUser(User user);

    void updateUser(User user);

    List<User> getAllUser();

    User getUserByLogin(String userLogin) throws NotFoundException;
}
