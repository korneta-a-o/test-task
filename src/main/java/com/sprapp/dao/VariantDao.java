package com.sprapp.dao;


import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Variant;

import java.util.List;

public interface VariantDao {

    void deleteVariant(Variant variant);

    void insertVariant(Variant variant);

    void updateVariant(Variant variant);

    List<Variant> getAllVariant();

    Variant getVariantById(int idVariant) throws NotFoundException;
}
