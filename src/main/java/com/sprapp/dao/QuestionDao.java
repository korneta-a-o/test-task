package com.sprapp.dao;

import com.sprapp.exception.NotFoundException;
import com.sprapp.model.Question;

import java.util.List;

public interface QuestionDao {

    void deleteQuestion(Question question);

    void insertQuestion(Question question);

    void updateQuestion(Question question);

    List<Question> getAllQuestion();

    List<Question> getQuestionsByIdTest(int idTest) throws NotFoundException;

}
