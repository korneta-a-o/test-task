package com.sprapp.dao;

import com.sprapp.model.Result;

import java.util.List;

public interface ResultDao {

    void insert(Result result);

    void update(Result result);

    void delete(Result result);

    Result selectById(int idResult);

    List<Result> getResults();
}
