package com.sprapp.util;

import com.sprapp.model.Question;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class QuestionsContainer {
    public        Map<Integer, SpecificListIterator<Question>> questionsCursorByTestId = new HashMap<>();
    private final Map<Integer, Integer>                        resultsByTestId         = new HashMap<>();

    public void startTest(Integer testId, Integer resultId, List<Question> questions) {
        questionsCursorByTestId.put(testId, new SpecificListIterator<>(questions));
        resultsByTestId.put(testId, resultId);
    }

    public Integer stopTest(Integer testId) {
        questionsCursorByTestId.remove(testId);
        return resultsByTestId.remove(testId);
    }

    public boolean isUserStartTest(Integer testId) {
        return questionsCursorByTestId.containsKey(testId);
    }

    public boolean hasQuestion(Integer testId) {
        return questionsCursorByTestId.get(testId).hasNext();
    }

    public void dropQuestion(Integer testId) {
        questionsCursorByTestId.get(testId).dropElement();
    }

    public Question getQuestion(Integer testId) {
        return questionsCursorByTestId.get(testId).getElement();
    }

    public void skipTest(Integer testId) {
        questionsCursorByTestId.get(testId).skipElement();
    }

    public Integer getResultId(Integer testId) {
        return resultsByTestId.get(testId);
    }

    private static class SpecificListIterator<T> {
        private List<T> list;
        private int     index;

        public SpecificListIterator(List<T> list) {
            this.list = list;
            index = 0;
        }

        public boolean hasNext() {
            return index < list.size();
        }

        public T getElement() {
            return list.get(index);
        }

        public int nextIndex() {
            return index + 1;
        }

        public void skipElement() {
            list.add(list.remove(index));
        }

        public void dropElement() {
            list.remove(index);
        }
    }

}
